﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


namespace TestTask
{
   

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        [DataContract]
        public partial class Capability
        {
            [DataMember]
            private CapabilityLayer layerField;

            /// <remarks/>
            [DataMember]
            public CapabilityLayer Layer
            {
                get
                {
                    return this.layerField;
                }
                set
                {
                    this.layerField = value;
                }
            }
        }

        /// <remarks/>
        [DataContract]
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class CapabilityLayer
        {
            [DataMember]
            private string nameField;
            [DataMember]
            private CapabilityLayerLayer[] layerField;
            [DataMember]
            private byte queryableField;

            /// <remarks/>
            [DataMember]
            public string Name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Layer")]
            [DataMember]
            public CapabilityLayerLayer[] Layer
            {
                get
                {
                    return this.layerField;
                }
                set
                {
                    this.layerField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public byte queryable
            {
                get
                {
                    return this.queryableField;
                }
                set
                {
                    this.queryableField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [DataContract]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class CapabilityLayerLayer
        {
            [DataMember]
            private string nameField;
            [DataMember]
            private string titleField;
            [DataMember]
            private CapabilityLayerLayerBoundingBox boundingBoxField;
            [DataMember]
            private CapabilityLayerLayerStyle styleField;
            [DataMember]
            private CapabilityLayerLayerAttribute[] attributesField;
            [DataMember]
            private string displayFieldField;
            [DataMember]
            private byte queryableField;
            [DataMember]
            private bool queryableFieldSpecified;
            [DataMember]
            private byte visibleField;

            /// <remarks/>
           [DataMember]
            public string Name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            public string Title
            {
                get
                {
                    return this.titleField;
                }
                set
                {
                    this.titleField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            public CapabilityLayerLayerBoundingBox BoundingBox
            {
                get
                {
                    return this.boundingBoxField;
                }
                set
                {
                    this.boundingBoxField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            public CapabilityLayerLayerStyle Style
            {
                get
                {
                    return this.styleField;
                }
                set
                {
                    this.styleField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("Attribute", IsNullable = false)]
            [DataMember]
            public CapabilityLayerLayerAttribute[] Attributes
            {
                get
                {
                    return this.attributesField;
                }
                set
                {
                    this.attributesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public string displayField
            {
                get
                {
                    return this.displayFieldField;
                }
                set
                {
                    this.displayFieldField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public byte queryable
            {
                get
                {
                    return this.queryableField;
                }
                set
                {
                    this.queryableField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlIgnoreAttribute()]
            [DataMember]
            public bool queryableSpecified
            {
                get
                {
                    return this.queryableFieldSpecified;
                }
                set
                {
                    this.queryableFieldSpecified = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public byte visible
            {
                get
                {
                    return this.visibleField;
                }
                set
                {
                    this.visibleField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [DataContract]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class CapabilityLayerLayerBoundingBox
        {
            [DataMember]
            private string cRSField;
            [DataMember]
            private decimal maxxField;
            [DataMember]
            private decimal minxField;
            [DataMember]
            private decimal maxyField;
            [DataMember]
            private decimal minyField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public string CRS
            {
                get
                {
                    return this.cRSField;
                }
                set
                {
                    this.cRSField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public decimal maxx
            {
                get
                {
                    return this.maxxField;
                }
                set
                {
                    this.maxxField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public decimal minx
            {
                get
                {
                    return this.minxField;
                }
                set
                {
                    this.minxField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public decimal maxy
            {
                get
                {
                    return this.maxyField;
                }
                set
                {
                    this.maxyField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public decimal miny
            {
                get
                {
                    return this.minyField;
                }
                set
                {
                    this.minyField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [DataContract]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class CapabilityLayerLayerStyle
        {
            [DataMember]
            private string nameField;
            [DataMember]
            private string titleField;
            [DataMember]
            private CapabilityLayerLayerStyleLegendURL legendURLField;

            /// <remarks/>
            [DataMember]
            public string Name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            public string Title
            {
                get
                {
                    return this.titleField;
                }
                set
                {
                    this.titleField = value;
                }
            }

            /// <remarks/>
           [DataMember]
            public CapabilityLayerLayerStyleLegendURL LegendURL
            {
                get
                {
                    return this.legendURLField;
                }
                set
                {
                    this.legendURLField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [DataContract]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class CapabilityLayerLayerStyleLegendURL
        {
            [DataMember]
            private string formatField;

            /// <remarks/>
            [DataMember]
            public string Format
            {
                get
                {
                    return this.formatField;
                }
                set
                {
                    this.formatField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [DataContract]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class CapabilityLayerLayerAttribute
        {
            [DataMember]
            private sbyte precisionField;
            [DataMember]
            private short lengthField;
            [DataMember]
            private string editTypeField;
            [DataMember]
            private string typeField;
            [DataMember]
            private string commentField;
            [DataMember]
            private string nameField;
            [DataMember]
            private string aliasField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public sbyte precision
            {
                get
                {
                    return this.precisionField;
                }
                set
                {
                    this.precisionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public short length
            {
                get
                {
                    return this.lengthField;
                }
                set
                {
                    this.lengthField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            [DataMember]
            public string editType
            {
                get
                {
                    return this.editTypeField;
                }
                set
                {
                    this.editTypeField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string comment
            {
                get
                {
                    return this.commentField;
                }
                set
                {
                    this.commentField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            [DataMember]
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string alias
            {
                get
                {
                    return this.aliasField;
                }
                set
                {
                    this.aliasField = value;
                }
            }
        }


    }

