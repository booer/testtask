﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace TestTask
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void DeSerializationButton_Click(object sender, EventArgs e)
        {
            
            Capability capability = new Capability();
            string path = Application.StartupPath + "\\..\\..\\Files\\Входные данные.xml";
            TextReader txtReader = new StreamReader(path);
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Capability));
            capability = (Capability)xmlSerializer.Deserialize(txtReader);
            txtReader.Close();

            path = Application.StartupPath + "\\..\\..\\Files\\OutputData.json";
            DataContractJsonSerializer formatter = new DataContractJsonSerializer(typeof(Capability));
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.WriteObject(fs, capability);
            }
            MessageBox.Show("JSON сериализация проведена");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string text;
            string path=Application.StartupPath + "\\..\\..\\Files\\OutputData.json";
            using (StreamReader sr = new StreamReader(path))
            {
                text=sr.ReadToEnd();
            }
            textBox1.Text = text;
        }
    }
}
